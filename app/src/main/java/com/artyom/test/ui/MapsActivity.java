package com.artyom.test.ui;

import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.artyom.test.R;
import com.artyom.test.events.ErrorEvent;
import com.artyom.test.events.EventBus;
import com.artyom.test.events.MapsSuccessEvent;
import com.artyom.test.service.Rest;
import com.artyom.test.utils.PermissionUtils;
import com.ahmadrosid.lib.drawroutemap.DrawMarker;
import com.ahmadrosid.lib.drawroutemap.DrawRouteMaps;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

public class MapsActivity extends AppCompatActivity
		implements OnMapReadyCallback, View.OnClickListener,
		ActivityCompat.OnRequestPermissionsResultCallback {

	private GoogleMap mMap;
	private int markerCount = 0;
	private Button path, line, reset;
	private TextView distance, duration;
	private ArrayList<LatLng> points = new ArrayList<>();
	/**
	 * Request code for location permission request.
	 *
	 * @see #onRequestPermissionsResult(int, String[], int[])
	 */
	private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

	/**
	 * Flag indicating whether a requested permission has been denied after returning in
	 * {@link #onRequestPermissionsResult(int, String[], int[])}.
	 */
	private boolean mPermissionDenied = false;
	private Rest rest;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_maps);

		findViews();
		initListeners();

		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);
		rest = new Rest(this);
	}

	/**
	 * find all views
	 */
	private void findViews() {
		path = (Button) findViewById(R.id.path);
		line = (Button) findViewById(R.id.line);
		reset = (Button) findViewById(R.id.reset);
		distance = (TextView)findViewById(R.id.distance);
		duration = (TextView)findViewById(R.id.duration);
	}

	/**
	 * initialize listeners
	 */
	private void initListeners() {
		path.setOnClickListener(this);
		line.setOnClickListener(this);
		reset.setOnClickListener(this);
	}

	@Override
	public void onMapReady(final GoogleMap googleMap) {
		mMap = googleMap;
		if (mMap != null) {
			mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
				@Override
				public void onMapClick(LatLng latLng) {
					if (markerCount == 2) {
						resetAllInfo();
					}
					addMarker(googleMap, latLng);
					points.add(latLng);
				}
			});
			enableMyLocation();
		}

	}

	/**
	 * draw real path
	 *
	 * @param points
	 */
	private void drawPath(ArrayList<LatLng> points) {

		DrawMarker.getInstance(this).draw(mMap, points.get(0), R.drawable.marker_a, "Origin Location");
		DrawMarker.getInstance(this).draw(mMap, points.get(1), R.drawable.marker_b, "Destination Location");
		DrawRouteMaps.getInstance(this)
				.draw(points.get(0), points.get(1), mMap);
		DrawMarker.getInstance(this).draw(mMap, points.get(0), R.drawable.marker_a, "Origin Location");
		DrawMarker.getInstance(this).draw(mMap, points.get(1), R.drawable.marker_b, "Destination Location");

		LatLngBounds bounds = new LatLngBounds.Builder()
				.include(points.get(0))
				.include(points.get(1)).build();
		Point displaySize = new Point();
		getWindowManager().getDefaultDisplay().getSize(displaySize);
		mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, displaySize.x, 250, 30));
	}

	/**
	 * draw straight line between points
	 *
	 * @param points
	 */
	private void drawLine(ArrayList<LatLng> points) {

		PolylineOptions line =
				new PolylineOptions();
		for (int i = 0; i < points.size(); i++) {
			line.add(points.get(i));
		}
		line.width(8).color(Color.RED);

		mMap.addPolyline(line);
	}


	/**
	 * add marker to map
	 *
	 * @param googleMap
	 * @param latLng
	 */
	private void addMarker(GoogleMap googleMap, LatLng latLng) {
		if (latLng != null) {
			googleMap.addMarker(new MarkerOptions().position(latLng));
			countMarker();
			path.setEnabled(markerCount == 2);
			line.setEnabled(markerCount == 2);
			reset.setEnabled(markerCount == 2);

		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.path:
				drawPath(points);
				Rest.getInstance().getMapsAditionalInformation(points.get(0).latitude + "," + points.get(0).longitude,
						points.get(1).latitude + "," + points.get(1).longitude);
				break;
			case R.id.line:
				drawLine(points);
				break;
			case R.id.reset:
				resetAllInfo();
				break;
		}
	}

	/**
	 * allow user add maximum two markers
	 */
	private void countMarker() {
		if (markerCount == 2) {
			markerCount = 1;
		} else {
			markerCount++;
		}
	}

	/**
	 * reset all data related to selected path
	 */
	private void resetAllInfo() {
		distance.setText("");
		duration.setText("");
		mMap.clear();
		markerCount = 0;
		points.clear();
		line.setEnabled(false);
		path.setEnabled(false);
		reset.setEnabled(false);
	}

	/**
	 * Displays a dialog with error message explaining that the location permission is missing.
	 */
	private void showMissingPermissionError() {
		PermissionUtils.PermissionDeniedDialog
				.newInstance(true).show(getSupportFragmentManager(), "dialog");
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions,
										   int[] grantResults) {
		if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
			return;
		}

		if (PermissionUtils.isPermissionGranted(permissions, grantResults,
				android.Manifest.permission.ACCESS_FINE_LOCATION)) {
			// Enable the my location layer if the permission has been granted.
			enableMyLocation();
		} else {
			// Display the missing permission error dialog when the fragments resume.
			mPermissionDenied = true;
		}
	}

	/**
	 * Enables the My Location layer if the fine location permission has been granted.
	 */
	private void enableMyLocation() {
		if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
				!= PackageManager.PERMISSION_GRANTED) {
			// Permission to access the location is missing.
			PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
					android.Manifest.permission.ACCESS_FINE_LOCATION, true);
		} else if (mMap != null) {
			// Access to the location has been granted to the app.
			mMap.setMyLocationEnabled(true);
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
	}

	@Override
	protected void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

	/**
	 * getting failer response event
	 * @param errorEvent
	 */
	@Subscribe
	public void onErrorEvent(ErrorEvent errorEvent) {
		Toast.makeText(this, errorEvent.getMessage(), Toast.LENGTH_SHORT).show();
	}

	/**
	 * getting event from success response
	 * @param mapsSuccessEvent
	 */
	@Subscribe
	public void onMapsSuccessEvent(MapsSuccessEvent mapsSuccessEvent) {
		duration.setText(mapsSuccessEvent.getRout().getLegs().get(0).getDuration().getText());
		distance.setText(mapsSuccessEvent.getRout().getLegs().get(0).getDistance().getText());
	}

}
