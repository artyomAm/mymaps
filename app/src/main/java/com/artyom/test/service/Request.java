package com.artyom.test.service;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class Request<T> implements Callback<T> {
	private Call<T> call;
	public Request(Call<T> call) {
		this.call = call;
	}
	protected Call<T> getCall() {
		return call;
	}

	public void enqueue() {
		call.enqueue(this);
	}

	public abstract void onExResponse(Call<T> call, Response<T> response);

	@Override
	public void onResponse(Call<T> call, Response<T> response) {
		onExResponse(call, response);
	}

	@Override
	public void onFailure(Call<T> call, Throwable t) {
		onExFailure(call, t);
		Log.d("DEBUG", "ERROR");
	}

	protected abstract void onExFailure(Call<T> call, Throwable t);
}
