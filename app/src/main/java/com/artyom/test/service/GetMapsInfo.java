package com.artyom.test.service;

import com.artyom.test.events.ErrorEvent;
import com.artyom.test.events.EventBus;
import com.artyom.test.events.MapsSuccessEvent;
import com.artyom.test.model.MapsResponse;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Artyom on 2/7/18.
 */

public class GetMapsInfo extends Request<MapsResponse> {
	public GetMapsInfo(RestInterface restInterface, String origin, String destination) {
		super(restInterface.getMapsInfo(origin, destination));
		enqueue();
	}

	@Override
	public void onExResponse(Call<MapsResponse> call, Response<MapsResponse> response) {
		if (response != null) {
			if (response.body().getRoute() != null && response.body().getRoute().size() == 0) {
				EventBus.getDefault().post(new ErrorEvent("there is no available rout between these locations"));
			} else if (response.body().getRoute() != null && response.body().getRoute().size() > 0) {
				EventBus.getDefault().post(new MapsSuccessEvent(response.body().getRoute().get(0)));
			}
		} else {
			EventBus.getDefault().post(new ErrorEvent("Could not load data for this rout"));
		}
	}

	@Override
	protected void onExFailure(Call<MapsResponse> call, Throwable t) {
		EventBus.getDefault().post(new ErrorEvent("Could not load data for this rout"));
	}
}
