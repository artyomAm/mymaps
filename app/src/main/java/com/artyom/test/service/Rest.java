package com.artyom.test.service;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Artyom on 2/6/18.
 */

public class Rest {
	private static Rest instance;
	private Context context;
	private String serverName;
	private String hostName;
	Retrofit retrofit;
	RestInterface restInterface;


	public Rest(Context context) {
		this.context = context;
		instance = this;
		serverName = "https://maps.googleapis.com/maps/";

		HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
		interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

		OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder()
				.connectTimeout(60, TimeUnit.SECONDS)
				.readTimeout(60, TimeUnit.SECONDS)
				.writeTimeout(60, TimeUnit.SECONDS)
				.addInterceptor(interceptor);

		OkHttpClient okHttpClient = okHttpBuilder.build();

		Gson gson = new GsonBuilder()
				.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
				.setLenient()
				.create();

		retrofit = new Retrofit.Builder()
				.baseUrl(serverName)
				.client(okHttpClient)
				.addConverterFactory(GsonConverterFactory.create(gson))
				.build();

		restInterface = retrofit.create(RestInterface.class);
	}

	public static synchronized Rest getInstance() {
		return instance;
	}
	public void getMapsAditionalInformation(String origin, String destination){
		new GetMapsInfo(restInterface,origin,destination);
	}
}
