package com.artyom.test.service;

import com.artyom.test.model.MapsResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Artyom on 2/7/18.
 */

public interface RestInterface {
	@GET("api/directions/json?key=AIzaSyC5X5gtiPtN1MKN5_P2bZ-8Ha7iVDj5CZY")
	Call<MapsResponse> getMapsInfo(@Query("origin") String origin, @Query("destination") String destination);

}
