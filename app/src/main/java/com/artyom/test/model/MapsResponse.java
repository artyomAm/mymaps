package com.artyom.test.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Artyom on 2/7/18.
 */

public class MapsResponse {
	@SerializedName("geocoded_waypoints")
	private ArrayList<GeocodedWaypoint> geocodedWaypoint;

	@SerializedName("routes")
	private ArrayList<Route> route;

	public MapsResponse(ArrayList<GeocodedWaypoint> geocodedWaypoint, ArrayList<Route> route) {
		this.geocodedWaypoint = geocodedWaypoint;
		this.route = route;
	}

	public ArrayList<GeocodedWaypoint> getGeocodedWaypoint() {
		return geocodedWaypoint;
	}

	public void setGeocodedWaypoint(ArrayList<GeocodedWaypoint> geocodedWaypoint) {
		this.geocodedWaypoint = geocodedWaypoint;
	}

	public ArrayList<Route> getRoute() {
		return route;
	}

	public void setRoute(ArrayList<Route> route) {
		this.route = route;
	}
}
