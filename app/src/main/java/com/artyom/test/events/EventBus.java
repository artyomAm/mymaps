package com.artyom.test.events;

import com.squareup.otto.Bus;

public class EventBus extends Bus {
	static volatile EventBus defaultInstance;

	public static EventBus getDefault() {
		if (defaultInstance == null) {
			synchronized (EventBus.class) {
				if (defaultInstance == null) {
					defaultInstance = new EventBus();
				}
			}
		}
		return defaultInstance;
	}
}
