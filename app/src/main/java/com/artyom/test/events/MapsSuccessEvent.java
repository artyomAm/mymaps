package com.artyom.test.events;

import com.artyom.test.model.Route;

/**
 * Created by Artyom on 2/7/18.
 */

public class MapsSuccessEvent {
	Route rout;

	public MapsSuccessEvent(Route rout) {
		this.rout = rout;
	}

	public Route getRout() {
		return rout;
	}
}
