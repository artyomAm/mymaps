package com.artyom.test.events;

/**
 * Created by Artyom on 2/7/18.
 */

public class ErrorEvent {
	private String message;

	public ErrorEvent(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
